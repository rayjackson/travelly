package meow.rayjackson.travelly.model;

/**
 * Created by rayjackson on 02.06.18.
 */

public class Route {

    private int id;
    private String name;
    private int cost;
    private int companyId;

    public Route(int id, String name, int cost, int companyId) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.companyId = companyId;
    }

    public Route(String name, int cost, int companyId) {
        this.name = name;
        this.cost = cost;
        this.companyId = companyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Route route = (Route) o;

        if (id != route.id) return false;
        if (cost != route.cost) return false;
        if (companyId != route.companyId) return false;
        return name != null ? name.equals(route.name) : route.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + cost;
        result = 31 * result + companyId;
        return result;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                ", companyId=" + companyId +
                '}';
    }
}

package meow.rayjackson.travelly.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rayjackson on 02.06.18.
 */

public class Point {

    private int id;
    private String address;
    private String name;
    private double latitude;
    private double longitude;
    private int routeId;

    public Point(int id, String address, String name, LatLng latLng, int routeId) {
        this.id = id;
        this.address = address;
        this.name = name;
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
        this.routeId = routeId;
    }

    public Point(String address, String name, LatLng latLng, int routeId) {
        this.address = address;
        this.name = name;
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
        this.routeId = routeId;
    }

    public Point(String address, String name, LatLng latLng) {
        this.address = address;
        this.name = name;
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }

    public Point(int id, String address, String name, double latitude, double longitude, int routeId) {
        this.id = id;
        this.address = address;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.routeId = routeId;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s", name, address);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {

        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {

        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (id != point.id) return false;
        if (Double.compare(point.latitude, latitude) != 0) return false;
        if (Double.compare(point.longitude, longitude) != 0) return false;
        if (routeId != point.routeId) return false;
        if (address != null ? !address.equals(point.address) : point.address != null) return false;
        return name != null ? name.equals(point.name) : point.name == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + routeId;
        return result;
    }

    public int getRouteId() {

        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }
}

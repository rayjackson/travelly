package meow.rayjackson.travelly.model;

/**
 * Created by rayjackson on 13.05.18.
 */

public class User {

    // basic properties
    private int id;
    private String login;
    private String password;

    private String firstName;
    private String lastName;

    /* if true => user is part of a travel companyId,
     * else user is a tourist.
     * ! false by default.
     */
    private boolean isTravelCompany;

    // companyId properties
    private int companyId;

    public User(int id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
        isTravelCompany = false;
    }

    public User(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        isTravelCompany = false;
    }

    public User(String login, String password, String firstName, String lastName, int companyId) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyId = companyId;
        isTravelCompany = true;
    }

    public User(int id, String login, String password, String firstName, String lastName, int companyId) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyId = companyId;
        isTravelCompany = true;
    }

    public User(int id, String login, String password, String firstName, String lastName) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        isTravelCompany = true;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isTravelCompany() {
        return isTravelCompany;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setTravelCompany(boolean travelCompany) {
        isTravelCompany = travelCompany;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isTravelCompany=" + isTravelCompany +
                ", companyId=" + companyId +
                '}';
    }
}

package meow.rayjackson.travelly.model;

/**
 * Created by rayjackson on 13.05.18.
 */

public class Company {

    private int id;
    private String name;
    private String code;

    public Company(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public Company(int id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package meow.rayjackson.travelly.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import meow.rayjackson.travelly.model.Company;
import meow.rayjackson.travelly.model.Route;
import meow.rayjackson.travelly.model.User;

/**
 * Created by rayjackson on 02.06.18.
 */

public class DBRouteProcessor extends SQLiteOpenHelper {

    //todo: fix me!

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_ROUTE = "Routes";

    public static final String ROUTE_ID = "_id";
    public static final String ROUTE_NAME = "name";
    public static final String ROUTE_PRICE = "price";
    public static final String ROUTE_COMPANY = "idCompany";

    public DBRouteProcessor(Context context) {
        super(context, DATABASE_ROUTE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_ROUTE + "("
                + ROUTE_ID + " integer primary key, "
                + ROUTE_NAME + " text, "
                + ROUTE_PRICE + " integer, "
                + ROUTE_COMPANY + " integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int addRoute(Route route) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROUTE_COMPANY, route.getCompanyId());
        database.insert(DATABASE_ROUTE, null, cv);
        List<Route> routes = findByCompanyId(route.getCompanyId());
        int id = routes.get(routes.size() - 1).getId();
        route.setId(id);
        return id;
    }

    public Route findById(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "SELECT * from " + DATABASE_ROUTE + " where "
                + ROUTE_ID + " = " + id;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(ROUTE_ID);
            int companyIndex = cursor.getColumnIndex(ROUTE_COMPANY);
            int nameIndex = cursor.getColumnIndex(ROUTE_NAME);
            int priceIndex = cursor.getColumnIndex(ROUTE_PRICE);

            do {
                int _id = cursor.getInt(idIndex);
                if (_id == id) {
                    String _name = cursor.getString(nameIndex);
                    int _price = cursor.getInt(priceIndex);
                    int _companyId = cursor.getInt(companyIndex);
                    Route route = new Route(_id, _name, _price, _companyId);
                    return route;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return null;
    }

    public List<Route> findByCompanyId(int companyId) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "SELECT * from " + DATABASE_ROUTE + " where "
                + ROUTE_COMPANY + " = " + companyId;
        Cursor cursor = database.rawQuery(query, null);

        List<Route> routes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(ROUTE_ID);
            int companyIndex = cursor.getColumnIndex(ROUTE_COMPANY);
            int nameIndex = cursor.getColumnIndex(ROUTE_NAME);
            int priceIndex = cursor.getColumnIndex(ROUTE_PRICE);

            do {
                int _companyId = cursor.getInt(companyIndex);
                if (_companyId == companyId) {
                    int _id = cursor.getInt(idIndex);
                    String _name = cursor.getString(nameIndex);
                    int _price = cursor.getInt(priceIndex);
                    Route route = new Route(_id, _name, _price, _companyId);
                    routes.add(route);
                }
            } while (cursor.moveToNext());
            cursor.close();
            return routes;
        }
        return null;
    }

    public List<Route> getAll() {
        SQLiteDatabase database = getWritableDatabase();
        String query = "SELECT * from " + DATABASE_ROUTE;
        Cursor cursor = database.rawQuery(query, null);
        List<Route> routes = new ArrayList<>();

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(ROUTE_ID);
            int companyIndex = cursor.getColumnIndex(ROUTE_COMPANY);
            int nameIndex = cursor.getColumnIndex(ROUTE_NAME);
            int priceIndex = cursor.getColumnIndex(ROUTE_PRICE);

            do {
                int _id = cursor.getInt(idIndex);
                    String _name = cursor.getString(nameIndex);
                    int _price = cursor.getInt(priceIndex);
                    int _companyId = cursor.getInt(companyIndex);
                    Route route = new Route(_id, _name, _price, _companyId);
                    routes.add(route);
            } while (cursor.moveToNext());
            cursor.close();
            return routes;
        }
        return null;

    }
}

package meow.rayjackson.travelly.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import meow.rayjackson.travelly.model.Company;

/**
 * Created by rayjackson on 13.05.18.
 */

public class DBCompanyProcessor extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE = "Companies";

    public static final String COMPANY_ID = "_id";
    public static final String COMPANY_NAME = "name";
    public static final String COMPANY_CODE = "code";

    public DBCompanyProcessor(Context context) {
        super(context, DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE + "("
                + COMPANY_ID + " integer primary key,"
                + COMPANY_NAME + " text,"
                + COMPANY_CODE + " text" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Company add(String name, String code) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COMPANY_NAME, name);
        cv.put(COMPANY_CODE, code);
        database.insert(DATABASE, null, cv);
        return findByCode(code);
    }

    public Company findByCode(String code) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "SELECT * from " + DATABASE + " WHERE " + COMPANY_CODE + " = '" + code + "'";
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(COMPANY_ID);
            int nameIndex = cursor.getColumnIndex(COMPANY_NAME);
            int codeIndex = cursor.getColumnIndex(COMPANY_CODE);

            do {
                String _code = cursor.getString(codeIndex);
                if(_code.equals(code)) {
                    int _id = cursor.getInt(idIndex);
                    String _name = cursor.getString(nameIndex);
                    Company company = new Company(_id, _name, _code);
                    return company;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return null;
    }

    public Company findById(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "SELECT * from " + DATABASE + " WHERE " + COMPANY_ID + " = " + id;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(COMPANY_ID);
            int nameIndex = cursor.getColumnIndex(COMPANY_NAME);
            int codeIndex = cursor.getColumnIndex(COMPANY_CODE);

            do {
                int _id = cursor.getInt(idIndex);
                if(_id == id) {
                    String _code = cursor.getString(codeIndex);
                    String _name = cursor.getString(nameIndex);
                    Company company = new Company(_id, _name, _code);
                    return company;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return null;
    }
}

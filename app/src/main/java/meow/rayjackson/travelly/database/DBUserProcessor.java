package meow.rayjackson.travelly.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import meow.rayjackson.travelly.model.Company;
import meow.rayjackson.travelly.model.User;

/**
 * Created by rayjackson on 13.05.18.
 */

public class DBUserProcessor extends SQLiteOpenHelper {

    //todo: fix me!

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_USER = "Users";

    public static final String USER_ID = "_id";
    public static final String USER_LOGIN = "login";
    public static final String USER_PWD = "password";
    public static final String USER_FIRST_NAME = "firstName";
    public static final String USER_LAST_NAME = "lastName";
    public static final String USER_COMPANY = "idCompany";

    public DBUserProcessor(Context context) {
        super(context, DATABASE_USER, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_USER + "("
                + USER_ID + " integer primary key,"
                + USER_LOGIN + " text,"
                + USER_PWD + " text,"
                + USER_FIRST_NAME + " text,"
                + USER_LAST_NAME + " text,"
                + USER_COMPANY + " integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addUser(User user) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_LOGIN, user.getLogin());
        cv.put(USER_PWD, user.getPassword());
        cv.put(USER_FIRST_NAME, user.getFirstName());
        cv.put(USER_LAST_NAME, user.getLastName());
        cv.put(USER_COMPANY, user.getCompanyId());
        database.insert(DATABASE_USER, null, cv);
        user.setId(findUserByLogin(user.getLogin()).getId());
    }

    public User findUserByLogin(String login) {
        SQLiteDatabase database = getReadableDatabase();
        String query = "SELECT * from " + DATABASE_USER;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(USER_ID);
            int loginIndex = cursor.getColumnIndex(USER_LOGIN);
            int pwdIndex = cursor.getColumnIndex(USER_PWD);
            int fnIndex = cursor.getColumnIndex(USER_FIRST_NAME);
            int lnIndex = cursor.getColumnIndex(USER_LAST_NAME);
            int cpIndex = cursor.getColumnIndex(USER_COMPANY);

            do {
                String _login = cursor.getString(loginIndex);
                if (_login.equals(login)) {
                    int _id = cursor.getInt(idIndex);
                    String _password = cursor.getString(pwdIndex);
                    String _firstName = cursor.getString(fnIndex);
                    String _lastName = cursor.getString(lnIndex);
                    int _companyId = cursor.getInt(cpIndex);
                    User user = new User(_id, _login, _password, _firstName, _lastName, _companyId);
                    return user;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return null;
    }

    public User findById(int id) {
        SQLiteDatabase database = getReadableDatabase();
        String query = "SELECT * from " + DATABASE_USER + " where " + USER_ID + " = " + id;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(USER_ID);
            int loginIndex = cursor.getColumnIndex(USER_LOGIN);
            int pwdIndex = cursor.getColumnIndex(USER_PWD);
            int fnIndex = cursor.getColumnIndex(USER_FIRST_NAME);
            int lnIndex = cursor.getColumnIndex(USER_LAST_NAME);
            int cpIndex = cursor.getColumnIndex(USER_COMPANY);

            do {
                int _id = cursor.getInt(idIndex);
                if (_id == id) {
                    String _login = cursor.getString(loginIndex);
                    String _password = cursor.getString(pwdIndex);
                    String _firstName = cursor.getString(fnIndex);
                    String _lastName = cursor.getString(lnIndex);
                    int _companyId = cursor.getInt(cpIndex);
                    return new User(_id, _login, _password, _firstName, _lastName, _companyId);
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return null;
    }

    public void deleteUserByLogin(String login) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "DELETE FROM " + DATABASE_USER + " WHERE " + USER_LOGIN + " = " + login;
        database.execSQL(query);
    }

    public void deleteUserById(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String query = "DELETE FROM " + DATABASE_USER + " WHERE " + USER_ID + " = " + id;
        database.execSQL(query);
    }

    public void updateUser(String login, User user) {
        deleteUserByLogin(login);
        addUser(user);
    }

    public void updateUser(int id, User user) {
        deleteUserById(id);
        user.setId(id);
        addUser(user);
    }

    public void eraseUsers() {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS " + DATABASE_USER);
        onCreate(database);
    }

    public List<User> getUsersByCompany(Company company) {
        SQLiteDatabase database = getReadableDatabase();
        String query = "SELECT * FROM " + DATABASE_USER
                + " WHERE " + USER_COMPANY + " = '" + company.getId() + "'";
        Cursor cursor = database.rawQuery(query, null);
        List<User> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(USER_ID);
            int loginIndex = cursor.getColumnIndex(USER_LOGIN);
            int pwdIndex = cursor.getColumnIndex(USER_PWD);
            int fnIndex = cursor.getColumnIndex(USER_FIRST_NAME);
            int lnIndex = cursor.getColumnIndex(USER_LAST_NAME);
            int cpIndex = cursor.getColumnIndex(USER_COMPANY);

            do {
                String _login = cursor.getString(loginIndex);
                int _id = cursor.getInt(idIndex);
                String _password = cursor.getString(pwdIndex);
                String _firstName = cursor.getString(fnIndex);
                String _lastName = cursor.getString(lnIndex);
                int _company = cursor.getInt(cpIndex);
                User user = new User(_id, _login, _password, _firstName, _lastName);
                result.add(user);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return result.size() > 0 ? result : null;
    }
}

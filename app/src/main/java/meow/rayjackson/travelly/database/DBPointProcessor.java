package meow.rayjackson.travelly.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import meow.rayjackson.travelly.model.Point;

/**
 * Created by rayjackson on 02.06.18.
 */

public class DBPointProcessor extends SQLiteOpenHelper {

    //todo: fix me!

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Places";

    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String LAT = "latitude";
    public static final String LONG = "longitude";
    public static final String ROUTE_ID = "routeId";

    public DBPointProcessor(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_NAME + "("
                + ID + " integer primary key, "
                + NAME + " text, "
                + ADDRESS + " text, "
                + LAT + " real, "
                + LONG + " real, "
                + ROUTE_ID + " integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int addPoint(Point point) {
        try (SQLiteDatabase database = getWritableDatabase()) {
            ContentValues cv = new ContentValues();
            cv.put(NAME, point.getName());
            cv.put(ADDRESS, point.getAddress());
            cv.put(LAT, point.getLatitude());
            cv.put(LONG, point.getLongitude());
            cv.put(ROUTE_ID, point.getRouteId());
            database.insert(DATABASE_NAME, null, cv);
            List<Point> pointsOfRoute = findByRouteId(point.getRouteId());
            int id = pointsOfRoute.get(pointsOfRoute.size() - 1).getId();
            point.setId(id);
            database.close();
            return id;
        }
    }

    public List<Point> findByRouteId(int routeId) {
        String query = "SELECT * from " + DATABASE_NAME + " where "
                + ROUTE_ID + " = " + routeId;
        try (SQLiteDatabase database = getWritableDatabase();
             Cursor cursor = database.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                int idIndex = cursor.getColumnIndex(ID);
                int nameIndex = cursor.getColumnIndex(NAME);
                int addressIndex = cursor.getColumnIndex(ADDRESS);
                int latIndex = cursor.getColumnIndex(LAT);
                int longIndex = cursor.getColumnIndex(LONG);
                int routeIndex = cursor.getColumnIndex(ROUTE_ID);

                List<Point> points = new ArrayList<>();

                do {
                    int _routeId = cursor.getInt(routeIndex);
                    if (_routeId == routeId) {
                        int _id = cursor.getInt(idIndex);
                        String _name = cursor.getString(nameIndex);
                        String _address = cursor.getString(addressIndex);
                        double _latitude = cursor.getDouble(latIndex);
                        double _longitude = cursor.getDouble(longIndex);
                        Point point = new Point(_id, _name, _address, _latitude, _longitude, _routeId);
                        points.add(point);
                    }
                } while (cursor.moveToNext());
                return points;
            }
            return null;
        }
    }

    public Point findById(int id) {
        String query = "SELECT * from " + DATABASE_NAME + " where "
                + ID + " = " + id;
        try (SQLiteDatabase database = getWritableDatabase();
             Cursor cursor = database.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                int idIndex = cursor.getColumnIndex(ID);
                int nameIndex = cursor.getColumnIndex(NAME);
                int addressIndex = cursor.getColumnIndex(ADDRESS);
                int latIndex = cursor.getColumnIndex(LAT);
                int longIndex = cursor.getColumnIndex(LONG);
                int routeIndex = cursor.getColumnIndex(ROUTE_ID);

                do {
                    int _id = cursor.getInt(idIndex);
                    if (_id == id) {
                        String _name = cursor.getString(nameIndex);
                        String _address = cursor.getString(addressIndex);
                        int _latitude = cursor.getInt(latIndex);
                        int _longitude = cursor.getInt(longIndex);
                        int _routeId = cursor.getInt(routeIndex);
                        return new Point(_id, _name, _address, _latitude, _longitude, _routeId);
                    }
                } while (cursor.moveToNext());
            }
            return null;
        }
    }
}

package meow.rayjackson.travelly;

import android.view.View;
import android.view.animation.Animation;

/**
 * Created by rayjackson on 18.03.18.
 */

public class ViewAnimationListener implements Animation.AnimationListener {
    private View view;
    /*
        mode = 1 => fadeOut;
        mode = 0 => fadeIn;
     */
    private int mode;

    public ViewAnimationListener(View view) {
        this.view = view;
        /* Fade in by default */
        this.mode = 0;
    }

    public ViewAnimationListener(View view, int mode) {
        this.view = view;
        /* Fade in by default */
        this.mode = mode;
    }


    @Override
    public void onAnimationStart(Animation animation) {
        if(mode == 0){
            this.view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(mode == 1){
            this.view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

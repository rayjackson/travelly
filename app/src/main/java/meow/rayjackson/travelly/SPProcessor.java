package meow.rayjackson.travelly;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by rayjackson on 13.05.18.
 */

public class SPProcessor {

    // finals
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";

    // vars
    private SharedPreferences sp;

    public SPProcessor(Context ctx) {
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public void login(String username) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(USER_NAME, username);
        editor.apply();
    }

    public String retrieveName() {
        return sp.getString(USER_NAME, "");
    }

    public boolean hasSession() {
        return !retrieveName().equals("");
    }

    public void logout() {
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(USER_NAME);
        editor.apply();
    }
}

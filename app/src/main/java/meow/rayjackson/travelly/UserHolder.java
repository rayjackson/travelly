package meow.rayjackson.travelly;

import meow.rayjackson.travelly.model.Company;
import meow.rayjackson.travelly.model.User;

/**
 * Created by rayjackson on 13.05.18.
 */

public class UserHolder {

    private static User user;

    public static void holdUser(User user) {
        UserHolder.user = user;
    }

    public static User getUser() {
        return user;
    }
}

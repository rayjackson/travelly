package meow.rayjackson.travelly.layouts.addRoute;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.model.Point;

/**
 * Created by rayjackson on 02.06.18.
 */

public class PlaceViewHolder extends RecyclerView.ViewHolder {

    private TextView name;
    private TextView address;

    public PlaceViewHolder(View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.place_name);
        address = itemView.findViewById(R.id.place_address);
    }

    public void bind(Point point) {
        name.setText(point.getName());
        address.setText(point.getAddress());
    }
}

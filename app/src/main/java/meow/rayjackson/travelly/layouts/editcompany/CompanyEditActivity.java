package meow.rayjackson.travelly.layouts.editcompany;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import meow.rayjackson.travelly.R;

public class CompanyEditActivity extends AppCompatActivity {

    private CompanyEditUI ui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_edit);
        ui = new CompanyEditUI(this);
    }
}

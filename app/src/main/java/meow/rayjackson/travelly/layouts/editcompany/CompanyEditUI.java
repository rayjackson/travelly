package meow.rayjackson.travelly.layouts.editcompany;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.database.DBCompanyProcessor;

/**
 * Created by rayjackson on 18.05.18.
 */

public class CompanyEditUI implements View.OnClickListener {

    private Activity activity;
    private DBCompanyProcessor dbProcessor;

    //Fields
    private EditText mName;
    private EditText mCode;
    private LinearLayout mCodeView;
    private Button mGenerate;
    private Button mContinue;

    public CompanyEditUI(Activity activity) {
        this.activity = activity;
        dbProcessor = new DBCompanyProcessor(activity);
        defineVars();
    }

    private void defineVars() {
        mName = (EditText) findView(R.id.company_name);
        mCode = (EditText) findView(R.id.company_code);
        mCodeView = (LinearLayout) findView(R.id.code_view);
        mGenerate = (Button) findView(R.id.edit_generate);
        mContinue = (Button) findView(R.id.edit_continue);
        mGenerate.setOnClickListener(this);
        mContinue.setOnClickListener(this);
    }

    private View findView(int resId) {
        return activity.findViewById(resId);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.edit_generate) {
            String name = mName.getText().toString();
            if (!name.equals("")) {
                CodeGenerator generator = new CodeGenerator();
                String code = generator.generate(name);
                registerCompany(name, code);
                mGenerate.setVisibility(View.GONE);
                mCodeView.setVisibility(View.VISIBLE);
                mCode.setText(code);
                addCodeToClipBoard(code);
            } else {
                Toast.makeText(activity, "Please check your inputs.", Toast.LENGTH_LONG).show();
            }
        } else if (v.getId() == R.id.edit_continue) {
            activity.onBackPressed();
        }

    }

    private void registerCompany(String name, String code) {
        dbProcessor.add(name, code);
    }

    private void addCodeToClipBoard(String code) {
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("code", code);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(activity, "Copied code to clipboard", Toast.LENGTH_SHORT).show();
    }

    class CodeGenerator {
        private String abc = "qwertyuiopasdfghjklzxcvbnm0123456789&!#@$%";

        // мин. вероятность создания одного и того же кода - 42^32, макс. - 42^64
        String generate(String s) {
            s = s.trim();
            // длина - от 32 до 64
            int iterations = (int) Math.round(Math.random() * 32 + 32);
            StringBuilder outcome = new StringBuilder();
            for (int i = 0; i < iterations; i++) {
                outcome.append(abc.charAt((int) Math.floor(Math.random() * abc.length())));
            }
            return outcome.toString();
        }

    }
}

package meow.rayjackson.travelly.layouts;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.SPProcessor;
import meow.rayjackson.travelly.UserHolder;
import meow.rayjackson.travelly.layouts.main.MainActivity;
import meow.rayjackson.travelly.model.User;
import meow.rayjackson.travelly.database.DBUserProcessor;
import meow.rayjackson.travelly.layouts.edituser.UserEditActivity;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = "LoginActivity";

    LinearLayout loginForm;
    LinearLayout registerForm;

    Button loginGo;
    Button registerGo;

    Button loginButton;
    Button registerButton;

    EditText logPassword;
    EditText logLogin;
    EditText regPassword;
    EditText regLogin;

    DBUserProcessor dbUserProcessor;
    SPProcessor spProcessor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        spProcessor = new SPProcessor(this);

        dbUserProcessor = new DBUserProcessor(this);
        if (spProcessor.hasSession()) {
            goToMain();
        }
        Log.d(TAG, "onCreate: "+dbUserProcessor);
//        dbUserProcessor.eraseUsers();

        defineVars();
        switchMode(2);

        registerGo.setOnClickListener(new GoListener(2));
        loginGo.setOnClickListener(new GoListener(1));

        ButtonListener buttonListener = new ButtonListener();
        registerButton.setOnClickListener(buttonListener);
        loginButton.setOnClickListener(buttonListener);
    }

    void defineVars() {
        loginForm = findViewById(R.id.loginForm);
        registerForm = findViewById(R.id.registerForm);

        loginGo = findViewById(R.id.loginGo);
        registerGo = findViewById(R.id.registerGo);

        regLogin = findViewById(R.id.loginEditText);
        regPassword = findViewById(R.id.passwordEditText);

        logLogin = findViewById(R.id.emailEditText);
        logPassword = findViewById(R.id.registerPasswordEditText);

        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);
    }

    void switchMode(int mode) {
        /*
            Loads form accordingly to mode:
                0 - Key Input,
                1 - Login Form,
                2 - Register Form.
        */

        switch (mode) {
            case 1:
                showLogin();
                break;
            case 2:
                showRegister();
                break;
        }
    }

    void showLogin() {
        registerForm.setVisibility(View.GONE);
        loginForm.setVisibility(View.VISIBLE);
    }

    void showRegister() {
        loginForm.setVisibility(View.GONE);
        registerForm.setVisibility(View.VISIBLE);
    }

    void howMany(SQLiteDatabase database) {
        Cursor cursor = database.query(DBUserProcessor.DATABASE_USER, null, null, null, null, null, null);
        int count = cursor.getCount();
        Log.d(TAG, "howMany: " + count);
        cursor.close();
    }

    boolean validateInputs(String login, String password) {
        return !(login == null || login.length() < 2 || password == null || password.length() < 8);
    }

    void readDatabase(SQLiteDatabase database) {
        Cursor cursor = database.query(DBUserProcessor.DATABASE_USER, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBUserProcessor.USER_ID);
            int loginIndex = cursor.getColumnIndex(DBUserProcessor.USER_LOGIN);
            int pwdIndex = cursor.getColumnIndex(DBUserProcessor.USER_PWD);
            do {
                Log.d(TAG, "readDatabase: { id: " + cursor.getInt(idIndex)
                        + " login: " + cursor.getString(loginIndex)
                        + " password: " + cursor.getString(pwdIndex) + " }");
            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    boolean searchDatabase(SQLiteDatabase database, String login, String password) {
        String query = "SELECT " + DBUserProcessor.USER_LOGIN + ", " + DBUserProcessor.USER_PWD
                + " from " + DBUserProcessor.DATABASE_USER;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBUserProcessor.USER_ID);
            int loginIndex = cursor.getColumnIndex(DBUserProcessor.USER_LOGIN);
            int pwdIndex = cursor.getColumnIndex(DBUserProcessor.USER_PWD);
            do {
                String _login = cursor.getString(loginIndex);
                String _password = cursor.getString(pwdIndex);
                Log.d(TAG, "searchDatabase: " + _login);
                if (_login.equals(login)) {
                    return password.equals(_password);
                }
            } while (cursor.moveToNext());
            cursor.close();
            return false;
        }
        return false;
    }

    void register(String login, String password) {
        SQLiteDatabase database = dbUserProcessor.getWritableDatabase();
        howMany(database);
        boolean isValid = validateInputs(login, password);
        boolean exist = dbUserProcessor.findUserByLogin(login) != null;
        if (isValid && !exist) {
            User user = new User(login, password, "", "");
            UserHolder.holdUser(user);
//            dbUserProcessor.addUser(user);
//            spProcessor.login(login);
            Intent intent = new Intent(this, UserEditActivity.class);
            startActivity(intent);
        } else if (exist) {
            snack("This username is already taken.");
        } else {
            snack("Please check your inputs.");
        }
    }

    void snack(String s) {
        Snackbar.make(
            findViewById(android.R.id.content),
            s,
            Snackbar.LENGTH_SHORT
        ).setAction("Action", null).show();

    }

    void signIn(String login, String password) {
        SQLiteDatabase database = dbUserProcessor.getWritableDatabase();
        boolean found = searchDatabase(database, login, password);
        Log.d(TAG, "login: " + found);
        if (found) {
            spProcessor.login(login);
            goToMain();
        } else {
            snack("Your login and password doesn't match in our database. "
                    + " Double-check your inputs and try again.");
        }
    }

    void goToMain() {
        String username = spProcessor.retrieveName();
        if (username != null) {
            User user = dbUserProcessor.findUserByLogin(username);
            Log.d(TAG, "goToMain: USER - " + user);
            UserHolder.holdUser(user);
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    class GoListener implements View.OnClickListener {
        private int mode;

        public GoListener() {
            this.mode = 1;
        }

        public GoListener(int mode) {
            this.mode = mode;
        }

        @Override
        public void onClick(View v) {
            switchMode(this.mode);
        }
    }

    class ButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String login;
            String password;
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            switch (v.getId()) {
                case R.id.registerButton:
                    login = regLogin.getText().toString();
                    password = regPassword.getText().toString();
                    register(login, password);
                    break;
                case R.id.loginButton:
                    login = logLogin.getText().toString();
                    password = logPassword.getText().toString();
                    signIn(login, password);
                    break;
            }
        }
    }
}

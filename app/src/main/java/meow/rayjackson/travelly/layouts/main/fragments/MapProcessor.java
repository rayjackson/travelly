package meow.rayjackson.travelly.layouts.main.fragments;

import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import meow.rayjackson.travelly.model.Point;

/**
 * Created by rayjackson on 06.05.18.
 */

public class MapProcessor implements GoogleMap.OnCameraMoveListener {

    private List<Circle> circles;
    private float zoom;
    private GoogleMap googleMap;


    public MapProcessor(GoogleMap googleMap) {
        this.googleMap = googleMap;
        circles = new ArrayList<>();
        googleMap.setOnCameraMoveListener(this);
    }

    public void moveCamera(GoogleMap googleMap, LatLng latLng, float zoom) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void moveCamera(GoogleMap googleMap, Location location, float zoom) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void addMarker(GoogleMap googleMap, Location location, String title) {
        googleMap.addMarker(
                new MarkerOptions()
                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title(title)
        );
    }

    public void addMarker(GoogleMap googleMap, LatLng latLng, String title) {
        googleMap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .title(title)
        );
    }

    public void addLine(GoogleMap googleMap, List<Point> points) {
        int color = randomColor();
        PolylineOptions options = new PolylineOptions().zIndex(5).color(color);

        for (Point point : points) {
            LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());
            options.add(latLng);
        }

        googleMap.addPolyline(options);

        for (Point point : points) {
            LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());
            Circle circle = googleMap.addCircle(new CircleOptions()
                    .center(latLng)
//                    .radius(10)
                    .zIndex(10)
                    .fillColor(color)
                    .strokeColor(color));
            circles.add(circle);
        }
    }

    private int randomColor() {
        Random random = new Random();
        int r = random.nextInt(256);
        int g = random.nextInt(256);
        int b = random.nextInt(256);
        System.out.println("Color : " + Color.rgb(r, g, b));
        return Color.rgb((int) r, (int) g, (int) b);
    }

    @Override
    public void onCameraMove() {
        float _zoom = googleMap.getCameraPosition().zoom;

        if (zoom != _zoom) {
            this.zoom = _zoom;

            for (Circle circle : circles) {
                float size = mapValue(zoom, 2.0f, 21.0f, 50.0f, 5.0f);
                System.out.println(size);
                circle.setRadius(size / 2);
            }
        }

    }

    private float mapValue(float value, float istart, float istop, float ostart, float ostop) {
        return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    }
}

package meow.rayjackson.travelly.layouts.main.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import meow.rayjackson.travelly.UserHolder;
import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.model.User;

public class UserFragment extends Fragment {

    public static final String TAG = "UserFragment";
    // vars
    private FragmentActivity fa;
    private TextView tvUserName;
    private TextView tvRealName;

    private OnFragmentInteractionListener mListener;

    public UserFragment() {
        // Required empty public constructor
    }

    public static UserFragment newInstance() {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        fa = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //TODO: add a name of a person to the title
        getActivity().setTitle(R.string.menu_account);
        defineVars();
    }

    @SuppressLint("SetTextI18n")
    void render(User user) {
        tvUserName.setText("@" + user.getLogin());
        tvRealName.setText(user.getFirstName() + " " + user.getLastName());
    }

    void defineVars() {
        tvUserName = fa.findViewById(R.id.tvUserName);
        tvRealName = fa.findViewById(R.id.tvRealName);
        User user = UserHolder.getUser();
        if (user != null) {
            render(user);
        }
    }
}

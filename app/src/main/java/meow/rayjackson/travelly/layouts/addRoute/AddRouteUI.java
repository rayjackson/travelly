package meow.rayjackson.travelly.layouts.addRoute;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.UserHolder;
import meow.rayjackson.travelly.database.DBCompanyProcessor;
import meow.rayjackson.travelly.database.DBPointProcessor;
import meow.rayjackson.travelly.database.DBRouteProcessor;
import meow.rayjackson.travelly.model.Company;
import meow.rayjackson.travelly.model.Point;
import meow.rayjackson.travelly.model.Route;
import meow.rayjackson.travelly.model.User;

import static android.app.Activity.RESULT_OK;

/**
 * Created by rayjackson on 02.06.18.
 */

public class AddRouteUI implements View.OnClickListener{
    private static final String TAG = "AddRouteUI";

    // Globals
    int PLACE_PICKER_REQUEST = 1;

    // Main
    private Activity activity;
    private DBCompanyProcessor companyProcessor;
    private DBRouteProcessor routeProcessor;
    private DBPointProcessor pointProcessor;

    // Fields
    private EditText mName;
    private TextView mCompany;
    private Button button;
    private ListView placeList;

    private List<Point> points;
    private Company company;

    AddRouteUI(Activity activity) {
        this.activity = activity;
        this.companyProcessor = new DBCompanyProcessor(activity);
        this.routeProcessor = new DBRouteProcessor(activity);
        this.pointProcessor = new DBPointProcessor(activity);
        this.points = new ArrayList<>();

        activity.setTitle("New route");

        defineVars();
        initViews();

        showCompany();
    }

    private void showCompany() {
        User user = UserHolder.getUser();
        company = companyProcessor.findById(user.getCompanyId());
        mCompany.setText(company.getName());
    }

    private void defineVars() {
        mName = (EditText) findView(R.id.route_name);
        mCompany = (TextView) findView(R.id.route_company);
        button = (Button) findView(R.id.route_add_place);
        placeList = (ListView) findView(R.id.place_list);
        button.setOnClickListener(this);
    }

    private View findView(int id) {
        return activity.findViewById(id);
    }

    private void initViews() {
        ArrayAdapter<Point> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, points);
        placeList.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.route_add_place) {
            Log.d(TAG, "onClick: i'm being clicked af!");
            startPlacePickerActivity();
        }
    }

    private void startPlacePickerActivity() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            activity.startActivityForResult(builder.build(activity), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    void onPlacePickerResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onPlacePickerResult: " + resultCode);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(activity, data);
                Point point = new Point(place.getName().toString(), place.getAddress().toString(), place.getLatLng());
                points.add(point);
                initViews();
            }
        }
    }

    void saveRoute() {
        if (points.size() > 0) {
            int id = pushRouteToDatabase();
            pushPlacesToDatabase(id);
        }
    }

    private int pushRouteToDatabase() {
        String name = mName.getText().toString();
        int price = 0;
        int companyId = company.getId();
        int id = routeProcessor.addRoute(new Route(name, price, companyId));

        Log.d(TAG, "pushRouteToDatabase: " + routeProcessor.findById(id));
        return id;
    }

    private void pushPlacesToDatabase(int routeId) {
        for (Point point : points) {
            point.setRouteId(routeId);
            int id = pointProcessor.addPoint(point);

            Log.d(TAG, "pushRouteToDatabase: " + pointProcessor.findById(id));
        }
    }


}

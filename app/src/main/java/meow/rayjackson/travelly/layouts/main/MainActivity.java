package meow.rayjackson.travelly.layouts.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import meow.rayjackson.travelly.database.DBUserProcessor;
import meow.rayjackson.travelly.layouts.LoginActivity;
import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.SPProcessor;
import meow.rayjackson.travelly.UserHolder;
import meow.rayjackson.travelly.layouts.main.fragments.OnFragmentInteractionListener;
import meow.rayjackson.travelly.layouts.main.fragments.TripsFragment;
import meow.rayjackson.travelly.layouts.main.fragments.UserFragment;
import meow.rayjackson.travelly.layouts.main.fragments.explore.ExploreFragment;
import meow.rayjackson.travelly.layouts.addRoute.AddRouteActivity;
import meow.rayjackson.travelly.model.User;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener {

    public static final String TAG = "MainActivity";

    private ActivityState activityState;
    private SPProcessor spProcessor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activityState = ActivityState.EXPLORE;

        spProcessor = new SPProcessor(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        displaySelectedFragment(R.id.menu_explore);
        activityState = ActivityState.EXPLORE;
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.menu_explore);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (activityState.equals(ActivityState.EXPLORE)) {
            User user = UserHolder.getUser();
            if (user == null) {
                String login = spProcessor.retrieveName();
                DBUserProcessor processor = new DBUserProcessor(this);
                user = processor.findUserByLogin(login);
                UserHolder.holdUser(user);
                System.out.println(login);
                System.out.println(user);
            }
            if (user.isTravelCompany()) {
                getMenuInflater().inflate(R.menu.explore_menu, menu);
            }
        } else {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add) {
            // Launch adding activity
            Intent intent = new Intent(this, AddRouteActivity.class);
            startActivity(intent);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

    /*
        if (id == R.id.menu_explore) {
            // Handle the camera action
        } else if (id == R.id.menu_trips) {

        } else if (id == R.id.menu_account) {

        }
    */

        displaySelectedFragment(id);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void displaySelectedFragment(int id) {
        Fragment fragment = null;

        switch (id) {
            case R.id.menu_explore:
                fragment = new ExploreFragment();
                activityState = ActivityState.EXPLORE;
                break;
            case R.id.menu_trips:
                fragment = new TripsFragment();
                activityState = ActivityState.TRIPS;
                break;
            case R.id.menu_account:
                fragment = new UserFragment();
                activityState = ActivityState.ACCOUNT;
                break;
            case R.id.menu_logout:
                spProcessor.logout();
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                // return so it wouldn't reach fragment transaction and collapse on that
                return;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_container, fragment).commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

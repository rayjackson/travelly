package meow.rayjackson.travelly.layouts.edituser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import meow.rayjackson.travelly.R;

public class UserEditActivity extends AppCompatActivity {

    private UserEditUI userEditUI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        userEditUI = new UserEditUI(this);
    }
}

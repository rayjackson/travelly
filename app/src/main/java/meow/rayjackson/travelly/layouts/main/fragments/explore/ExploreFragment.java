package meow.rayjackson.travelly.layouts.main.fragments.explore;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.database.DBPointProcessor;
import meow.rayjackson.travelly.database.DBRouteProcessor;
import meow.rayjackson.travelly.layouts.main.fragments.MapProcessor;
import meow.rayjackson.travelly.layouts.main.fragments.OnFragmentInteractionListener;
import meow.rayjackson.travelly.model.Point;
import meow.rayjackson.travelly.model.Route;

/* Google Places API Key:  AIzaSyD06Qwr8c0jesMk_8rFGKAnkzo4y1c3Z7M   */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "ExploreFragment";
    private static final float DEFAULT_ZOOM = 15;
    Location currentLocation;

    /**
     * vars:
     **/
    private OnFragmentInteractionListener mListener;
    private FragmentActivity fa;
    private GoogleMap googleMap;
    private MapProcessor mapProcessor;
    // user's current location
    private FusedLocationProviderClient fusedLocationProviderClient;

    //widgets
    private EditText searchEditText;

    public ExploreFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ExploreFragment newInstance(String param1, String param2) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle(R.string.menu_explore);
        initializeUI();
        initializeMap();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapProcessor != null) {
            Log.d(TAG, "onResume: ");
            setMapInfo();
        }
    }

    @Override
    public void onAttach(Context context) {
        fa = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void initializeMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.explore_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap _googleMap) {
        googleMap = _googleMap;
        getDeviceLocation();
        if (ActivityCompat.checkSelfPermission(fa, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fa, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        mapProcessor = new MapProcessor(googleMap);

        setMapInfo();

        mapProcessor.onCameraMove();
    }

    private void getDeviceLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(fa);
        if (ActivityCompat.checkSelfPermission(fa, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fa, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        final Task location = fusedLocationProviderClient.getLastLocation();
        location.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "onComplete: found location");
                    Location currentLocation = (Location) task.getResult();
                    saveLocation(currentLocation);
                    mapProcessor.moveCamera(googleMap, currentLocation, DEFAULT_ZOOM);
                } else {
                    Log.d(TAG, "onComplete: unable to find location");
                }
            }
        });
    }

    private void saveLocation(Location location) {
        currentLocation = location;
    }

    private void initializeUI() {
        searchEditText = fa.findViewById(R.id.explore_search);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        || event.getAction() == KeyEvent.KEYCODE_ENTER) {
                    LatLng searchBounds = search();
                    if (searchBounds != null) {
                        // do something;
                    }
                }
                return false;
            }
        });
    }

    private LatLng search() {
        String search = searchEditText.getText().toString();
        Log.d(TAG, "search: searching for " + search);
        Geocoder geocoder = new Geocoder(fa, Locale.getDefault());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(search, 1);
        } catch (IOException e) {
            /** Geocode may not work sometimes if 'NetworkLocator' has been killed.
             This can be solved by reboot only (as far as I know).
             */
            Log.d(TAG, "error: " + e.getLocalizedMessage());
        }

        if (list.size() > 0) {
            Address address = list.get(0);
            Toast.makeText(fa, address.getAddressLine(0), Toast.LENGTH_SHORT).show();
            return new LatLng(address.getLatitude(), address.getLongitude());
        }
        Log.d(TAG, "search: unable to find " + search);
        return null;
    }

    private void setMapInfo() {
        googleMap.clear();
        DBRouteProcessor routeProcessor = new DBRouteProcessor(fa);
        List<Route> routes = routeProcessor.getAll();
        Log.d(TAG, "setMapInfo: " + routes);
        for (Route route : routes) {
            drawRoute(route);
        }

    }

    private void drawRoute(Route route) {
        DBPointProcessor pointProcessor = new DBPointProcessor(fa);
        List<Point> points = pointProcessor.findByRouteId(route.getId());
        Log.d(TAG, "drawRoute: " + points);
        if(points != null) {
            mapProcessor.addLine(googleMap, points);
        }
    }


}

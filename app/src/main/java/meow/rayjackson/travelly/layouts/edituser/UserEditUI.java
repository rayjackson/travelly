package meow.rayjackson.travelly.layouts.edituser;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import meow.rayjackson.travelly.R;
import meow.rayjackson.travelly.UserHolder;
import meow.rayjackson.travelly.layouts.editcompany.CompanyEditActivity;
import meow.rayjackson.travelly.model.Company;
import meow.rayjackson.travelly.layouts.main.MainActivity;
import meow.rayjackson.travelly.model.User;
import meow.rayjackson.travelly.database.DBCompanyProcessor;
import meow.rayjackson.travelly.database.DBUserProcessor;

/**
 * Created by rayjackson on 13.05.18.
 */

public class UserEditUI implements View.OnClickListener {

    public static final String TAG = "UserEditUI";

    private User user;
    private View layout;
    private EditText firstName;
    private EditText lastName;
    private EditText companyCode;
    private Button create;
    private Button button;

    private Activity activity;
    private DBUserProcessor dbUserProcessor;
    private DBCompanyProcessor dbCompanyProcessor;

    public UserEditUI(Activity activity) {
        this.activity = activity;
        layout = findView(R.id.edit_layout);
        layout.setPadding(layout.getPaddingLeft(), layout.getPaddingTop(), layout.getPaddingRight(), getNavBarHeight() + layout.getPaddingBottom());
        firstName = (EditText) findView(R.id.edit_first_name);
        lastName = (EditText) findView(R.id.edit_last_name);
        companyCode = (EditText) findView(R.id.edit_company);
        button = (Button) findView(R.id.edit_button);
        button.setOnClickListener(this);
        create = (Button) findView(R.id.edit_create_company);
        create.setOnClickListener(this);
        dbUserProcessor = new DBUserProcessor(activity);
        dbCompanyProcessor = new DBCompanyProcessor(activity);
    }

    private View findView(int idRes) {
        return activity.findViewById(idRes);
    }

    private String getString(EditText editText) {
        return editText.getText().toString();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.edit_button) {
            String sFirstName = getString(firstName);
            String sLastName = getString(lastName);
            boolean isValid = validate(sFirstName, sLastName);
            if (isValid) {
                complete();
            }
        } else if (v.getId() == R.id.edit_create_company) {
            Intent intent = new Intent(activity, CompanyEditActivity.class);
            activity.startActivity(intent);
        }
    }

    private void complete() {
        user = UserHolder.getUser();
        boolean isTravelCompany = false;
        user.setFirstName(getString(firstName));
        user.setLastName(getString(lastName));
        String sCompanyCode = getString(companyCode);
        if (!sCompanyCode.equals("")) {
            Company company = dbCompanyProcessor.findByCode(sCompanyCode);
            if (company != null) {
                isTravelCompany = true;
                user.setCompanyId(company.getId());
                List<User> s = dbUserProcessor.getUsersByCompany(company);
                if (s != null) {
                    Log.d(TAG, "complete: " + s.toString());
                }
//                return;
            } else {
//                Toast.makeText(activity, "Company with this code doesn't exist yet", Toast.LENGTH_LONG).show();
                Snackbar.make(
                        activity.findViewById(android.R.id.content),
                        "Company with this code doesn't exist yet",
                        Snackbar.LENGTH_SHORT
                ).setAction("Action", null).show();
                return;
            }
        }
        user.setTravelCompany(isTravelCompany);
        addUserToDB(user);

        UserHolder.holdUser(user);
        goToMain();
    }

    private void goToMain() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private boolean validate(String sFirstName, String sLastName) {
        if (sFirstName == null || sFirstName.equals("") || sLastName == null || sLastName.equals("")) {
            Toast.makeText(activity, "Please check your inputs", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void addUserToDB(User user) {
        Log.d(TAG, "addUserToDB: " + dbUserProcessor.findUserByLogin(user.getLogin()));
        dbUserProcessor.addUser(user);
        Log.d(TAG, "addedUserToDB: " + dbUserProcessor.findUserByLogin(user.getLogin()));
    }

    // A method to find the height of the navigation bar
    private int getNavBarHeight() {
        int result = 0;
        int resId = activity.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resId > 0) {
            result = activity.getResources().getDimensionPixelSize(resId);
        }
        Log.d(TAG, "getNavBarHeight: " + result);
        return result;
    }
}

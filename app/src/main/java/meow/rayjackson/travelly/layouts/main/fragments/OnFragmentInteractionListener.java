package meow.rayjackson.travelly.layouts.main.fragments;

import android.net.Uri;

/**
 * Created by rayjackson on 18.03.18.
 */

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);
}

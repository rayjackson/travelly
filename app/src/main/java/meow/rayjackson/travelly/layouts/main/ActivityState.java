package meow.rayjackson.travelly.layouts.main;

/**
 * Created by rayjackson on 13.05.18.
 */

public enum ActivityState {
    EXPLORE, TRIPS, ACCOUNT
}
